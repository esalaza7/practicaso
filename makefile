CXXFLAGS=-std=c++11

all: control interpreter

control: src/control.o src/interpreter.o
	$(CXX) -o $@ $^ -lrt -lpthread $(CXXFLAGS)

interpreter: src/mainInterpreter.o src/interpreter.o
	$(CXX) -o $@ $^ -lrt -lpthread $(CXXFLAGS)

control.o: src/control.cpp src/control.h src/interpreter.h

mainInterpreter.o: src/mainInterpreter.cpp src/interpreter.h

interpreter.o: src/interpreter.cpp src/interpreter.h

clean:
	rm -f control interpreter
	rm -f src/*.o src/*.*~ src/*~
